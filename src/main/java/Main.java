// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.stream.StreamSource;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;

/**
 * An example of a client using the merge REST API from commandline. The requests/responses are handled
 * as XML Strings (and processed using XPath queries).
 *
 * This example is designed to provide similar capabilities to that of the XML Merge command-line driver
 * that interacts via the Java API while this code provides similar capabilities using calls to the rest server.
 *
 * The REST server must be running for this code to operate.  It connects to localhost for the REST service.
 * The host can be configured using system property 'host'.
 */
public class Main {

	// merge commands
	private static final String DESCRIBE = "describe";
	private static final String MERGE = "merge";

	// merge-type definitions
	private static final String CONCURRENT_MERGE_TYPE = "concurrent";
	private static final String THREE_WAY_CONCURRENT_MERGE_TYPE = "concurrent3";
	private static final String THREE_WAY_MERGE_TYPE = "threeway";
	private static final String SEQUENTIAL_MERGE_TYPE = "sequential";
	
	private static WebTarget target;
	private static Processor p;
	
	private static int argsLength=0;
	private static int firstParamIndex;
	private static int paramCount;
	private static int resultFileLocation;

	public static void main(String[] args) throws Exception {

		Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

		String host = "http://0.0.0.0:8080";
		if (System.getProperty("host") != null) {
			host = System.getProperty("host");
		}

		target = client.target(host + "/api/xmlmerge/v1");
		p = new Processor(false);

		//first argument is either describe or merge command and second argument is merge type
		if (args.length == 0) {
			usage();
		} else {
			String command = args[0].toLowerCase();
			switch (command) {
			case DESCRIBE:
				if (args.length > 1) {
					String mergeType = args[1].toLowerCase();
					if (THREE_WAY_CONCURRENT_MERGE_TYPE.equals(mergeType)) {
						mergeType= THREE_WAY_MERGE_TYPE;
					}
					switch (mergeType) {
					case CONCURRENT_MERGE_TYPE:
						describeMergeType(target, p, CONCURRENT_MERGE_TYPE);
						break;
					case THREE_WAY_MERGE_TYPE:
						describeMergeType(target, p, THREE_WAY_MERGE_TYPE);
						break;
					case SEQUENTIAL_MERGE_TYPE:
						describeMergeType(target, p, SEQUENTIAL_MERGE_TYPE);
						break;
					default:
						System.err.println("\n ERROR: Unknown Merge type '" + mergeType + "'");
						usage();
						System.exit(1);
					}
				} else {
					System.err.println("ERROR: Not enough arguments to '" + command + "'");
					usage();
					System.exit(1);
				}
				break;
			case MERGE:
				if (args.length > 1) {
					String mergeType = args[1].toLowerCase();
					if (THREE_WAY_CONCURRENT_MERGE_TYPE.equals(mergeType)) {
						mergeType= THREE_WAY_MERGE_TYPE;
					}
					switch (mergeType) {
					case CONCURRENT_MERGE_TYPE:
						runMerge(target, p, CONCURRENT_MERGE_TYPE, args);
						break;
					case THREE_WAY_MERGE_TYPE:
						runMerge(target, p,  THREE_WAY_MERGE_TYPE, args);
						break;
					case SEQUENTIAL_MERGE_TYPE:
						runMerge(target, p, SEQUENTIAL_MERGE_TYPE, args);
						break;
					default:
						System.err.println("\n ERROR: Unknown Merge type '" + mergeType + "'");
						usage();
						System.exit(1);
					}
				} else {
					System.err.println("ERROR: Not enough arguments to '" + command + "'");
					usage();
					System.exit(1);
				}
				break;
			default:
				System.err.println("\n ERROR: Unknown Merge command '" + command + "'");
				usage();
				System.exit(1);
			}
		}
	}

	/**
	 * Lists the merge types and its usage to System.out
	 */
	private static void usage() {

		System.out.println();
		System.out.println(" Usage:");
		System.out.println(" java -jar deltaxml-merge-rest-client-x.y.z.jar");
		System.out.println(" java -Dhost=http://localhost:1234 -jar deltaxml-merge-rest-client-x.y.z.jar");
		System.out.println(" java -jar deltaxml-merge-rest-client-x.y.z.jar describe <mergeType>"
				+ " to see the description of the available parameters for the specified merge type.");
		System.out.println(" java -jar deltaxml-merge-rest-client-x.y.z.jar merge <mergeType>"
				+ " <ancestorName/version1Name> <ancestorFile/version1File> (<versionName> <versionFile>)+ <resultFile> <params>");
		System.out.println();
		System.out.println(" Available Merge Types:");
		System.out.println(" ====================================================");
		System.out.println(" Merge Type        | Short Description");
		System.out.println(" ====================================================");
		System.out.println(" concurrent        | N-Way concurrent merge");
		System.out.println(" concurrent3       | Three way concurrent merge");
		System.out.println(" sequential        | N-Way sequential merge");
		System.out.println(" ====================================================");
		System.out.println();
	}

	/**
	 * Lists the configuration for a merge type using GET on the /types/mergeType resource
	 * 
	 * @param target client info
	 * @param p a saxon Processor
	 * @param mergeType mergeType a merge type from the commandline
	 * @throws SaxonApiException
	 */
	private static void describeMergeType(WebTarget target, Processor p, String mergeType) throws SaxonApiException {

		if (CONCURRENT_MERGE_TYPE.equals(mergeType)) {
			System.out.println(
					"\n A concurrent merge requires an ancestor file and at least two revision files. Each file needs a name.");
			System.out.println("\n java -jar deltaxml-merge-rest-client-x.y.z.jar merge " + mergeType
					+ " <ancestorName> <ancestorFile> (<versionName> <versionFile>)+ <resultFile> <params>\n");
		} else if (THREE_WAY_MERGE_TYPE.equals(mergeType)) {
			System.out.println(
					"\n A three way concurrent merge requires an ancestor file and two revision files. Each file needs a name.");
			System.out.println("\n java -jar deltaxml-merge-rest-client-x.y.z.jar merge " + mergeType
					+ " <ancestorName> <ancestorFile> <version1Name> <version1File> <version2Name> <version2File> <resultFile> <params>\n");
		} else if (SEQUENTIAL_MERGE_TYPE.equals(mergeType)) {
			System.out.println("\n A sequential merge requires at least three revision files. Each file needs a name.");
			System.out.println("\n java -jar deltaxml-merge-rest-client-x.y.z.jar merge " + mergeType
					+ " (<versionName> <versionFile>)+ <resultFile> <params>\n");
		}

		System.out.println(" The following are the allowed params and their default values:");
		//requesting information for a merge type
		Response response = getResponse("/types/" + mergeType, target);
		if (response != null) {
			String params = response.readEntity(String.class);
			XdmNode n = p.newDocumentBuilder().build(new StreamSource(new StringReader(params)));
			long paramCount = getLongByXPath(p, n, "count(/*/Configuration/*)");

			System.out.print(String.format(" %-35s%-35s\n", "=", "=").replace(" ", "="));
			System.out.printf(" %-35s | %s\n", "Param", "Default Values");
			System.out.print(String.format(" %-35s%-35s\n", "=", "=").replace(" ", "="));

			//getting configuration/resultType for a merge type
			String resultypeXPath = "/*/ResultType";
			String resultTypeName = getStringByXPath(p, n, "name(" + resultypeXPath + ")");
			String resultTypeDefault = getStringByXPath(p, n, resultypeXPath);
			System.out.printf(" %-35s | %s\n", resultTypeName, resultTypeDefault);
			System.out.print(String.format(" %-35s%-35s\n", "-", "-").replace(" ", "-"));

			for (long l = 1; l <= paramCount; l++) {
				String xPath = "/*/Configuration/*[%d]";
				String name = getStringByXPath(p, n, String.format("name(" + xPath + ")", l));
				String defaultValue = getStringByXPath(p, n, String.format(xPath, l));
				if (!name.equals("RuleConfiguration")) {
					System.out.printf(" %-35s | %s\n", name, defaultValue);
					if (l < paramCount) {
						System.out.print(String.format(" %-35s%-35s\n", "-", "-").replace(" ", "-"));
					}
				}
			}
			System.out.print(String.format(" %-35s%-35s\n", "=", "=").replace(" ", "="));

			System.out.println(
					"\n See documentation at https://docs.deltaxml.com/xml-merge/latest/xml-merge-rest-user-guide/requests/specifying-parameters for more details about parameters and its values.\n");
		}
	}
	
	/**
	 * This function runs the appropriate merge based the merge type using POST
	 * 
	 * @param target client info
	 * @param p saxon processor
	 * @param mergeType a merge type from the commandline
	 * @param args arguments from the commandline
	 * @throws Exception
	 */
	private static void runMerge(WebTarget target, Processor p, String mergeType, String[] args) throws Exception {
		
		//validate arguments
		checkArguments(mergeType, args);
		
		//get multipart data for a multiPart form request
		MultiPart multiPart = getMultipartEntity(mergeType, args);
        
		//submitting comparison reguest
		Response response = target.path("/types/"+ mergeType).request().post(Entity.entity(multiPart, multiPart.getMediaType()));
        
        if (response.getStatus() == 202) {
        	//merge request accepted
        	URI jobURI = response.getLocation();
            String jobSuffix = jobURI.getPath().replace("/api/xmlmerge/v1/", "");
            boolean finished = false;
            String previousState = "";
            String previousPhaseDescription = "";
            String jobState = null;
            String phaseDescription = null;
            
            System.out.print("\n Progress:  \n");
            
            while (!finished) {
            	//getting corresponding job for the merge request
                response =getResponse(jobSuffix, target);
                String jobResponse = response.readEntity(String.class);
                XdmNode jobResult = p.newDocumentBuilder().build(new StreamSource(new StringReader(jobResponse)));
                
                //extracting jobStatus and phaseDescription from the job response
                jobState = getStringByXPath(p, jobResult, "/job/jobStatus");
                if (jobResponse.contains("phaseDescription")) {
                	phaseDescription = getStringByXPath(p, jobResult, "/job/phaseDescription");
                }
                finished = "SUCCESS".equals(jobState) || "FAILED".equals(jobState);
                if (!previousState.equals(jobState) && !jobState.equals("FAILED")) {
                	System.out.println("	" + jobState);
                    previousState = jobState;
                }
                if (phaseDescription!=null && !previousPhaseDescription.equals(phaseDescription) && !jobState.equals("FAILED")) {
                	System.out.println("		" + phaseDescription);
                	previousPhaseDescription = phaseDescription;
                }
            }
            
            System.out.println();
            
            if ("FAILED".equals(jobState)) {
                System.out.println(" MERGE FAILED.");
                
                //getting corresponding job for the merge request
                response =getResponse(jobSuffix, target);
                String xmlResult = response.readEntity(String.class);
                XdmNode resultTree = p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlResult)));
                
                //extracting error message from the job response
                String errorMessage = getStringByXPath(p, resultTree, "/job/error/ErrorMessage");
                System.err.printf("\n %-20s\n\n", "ERROR: " + errorMessage);
                describeMergeType(target, p, mergeType);
                System.exit(1);
            } else {
                System.out.println(" Result written to " + args[resultFileLocation]);
                System.exit(0);
            }
            
        } else {
        	//merge request failed
        	System.err.println(" Merge failed with error code " + response.getStatus());
        	String xmlResult = response.readEntity(String.class);
        	XdmNode resultTree = p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlResult)));
            String errorMessage = getStringByXPath(p, resultTree, "/*/ErrorMessage");
            System.err.printf("\n %-20s\n\n", "ERROR: " + errorMessage);
            describeMergeType(target, p, mergeType);
			System.exit(1);
        }
	}

	/**
	 * This functions returns the multipart entity for a merge request
	 * 
	 * @param mergeType a merge type from the commandline
	 * @param args arguments from the commandline
	 * @return the multipart entity
	 */
	private static MultiPart getMultipartEntity(String mergeType, String[] args) throws Exception {
		List<String> versionOrder= new ArrayList<>();
		List<FormDataBodyPart> formDataBodyPartList= new ArrayList<>();
		
		//adding version names and version inputs 
		if (THREE_WAY_MERGE_TYPE.equals(mergeType)) {
			formDataBodyPartList.add(new FormDataBodyPart("AncestorName", args[2], MediaType.TEXT_PLAIN_TYPE));
			formDataBodyPartList.add(new FormDataBodyPart("Ancestor", args[3], MediaType.TEXT_PLAIN_TYPE));
			formDataBodyPartList.add(new FormDataBodyPart("VersionOneName", args[4], MediaType.TEXT_PLAIN_TYPE));
			formDataBodyPartList.add(new FormDataBodyPart("VersionOne", args[5], MediaType.TEXT_PLAIN_TYPE));
			formDataBodyPartList.add(new FormDataBodyPart("VersionTwoName", args[6], MediaType.TEXT_PLAIN_TYPE));
			formDataBodyPartList.add(new FormDataBodyPart("VersionTwo", args[7], MediaType.TEXT_PLAIN_TYPE));
		} else {
			for (int i=2; i < resultFileLocation; i+= 2) {
				String version=args[i];
				String input=args[i+1];
				formDataBodyPartList.add(new FormDataBodyPart(version, input, MediaType.TEXT_PLAIN_TYPE));
				versionOrder.add(version);
			}
			formDataBodyPartList.add(new FormDataBodyPart("VersionOrder", String.join(",", versionOrder), MediaType.TEXT_PLAIN_TYPE));
		}
		
		//adding parameters
		if(paramCount > 0) {
			for (int i=firstParamIndex; i < argsLength; i++) {
				String[] param=args[i].split("=");
				checkParameter(mergeType, param);
				formDataBodyPartList.add(new FormDataBodyPart(param[0], param[1], MediaType.TEXT_PLAIN_TYPE));
			}
		}
		
		MultiPart multiPart = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE);

		for (FormDataBodyPart formDataBodyPart : formDataBodyPartList) {
			multiPart.bodyPart(formDataBodyPart);
		}
		
		//asynchronous merge request
		multiPart.bodyPart(new FormDataBodyPart("Async", "true", MediaType.TEXT_PLAIN_TYPE));
		multiPart.bodyPart(new FormDataBodyPart("AsyncOutput", args[resultFileLocation], MediaType.TEXT_PLAIN_TYPE));
		return multiPart;
	}

	/**
	 * This functions validates the parameters passed from commandline
	 * 
	 * @param mergeType a merge type from the commandline
	 * @param param a parameter array of size 2 with its name and value
	 * @throws SaxonApiException Exception thrown by Saxon
	 */
	private static void checkParameter(String mergeType, String[] param) throws SaxonApiException {
		if (param.length == 2) {
			if ("".equals(param[0])) {
				 System.err.println("\n ERROR: Parameter name is not present: '" + param[0] + "'");
				 describeMergeType(target, p, mergeType);
				 System.exit(1);
			}
			if ("".equals(param[1])) {
				 System.err.println("\n ERROR: Parameter value is not present: '" + param[1] + "'");
				 describeMergeType(target, p, mergeType);
				 System.exit(1);
			}
		} else {
			System.err.println("\n ERROR: Parameter and its value should be in the form param=value");
			describeMergeType(target, p, mergeType);
			System.exit(1);
		}
		
		Response response = getResponse("/types/" + mergeType, target);
		String params = response.readEntity(String.class);
		XdmNode n = p.newDocumentBuilder().build(new StreamSource(new StringReader(params)));
		long paramCount = getLongByXPath(p, n, "count(/*/Configuration/*)");
		
		String resultypeXPath = "/*/ResultType";
		String resultTypeName = getStringByXPath(p, n, "name(" + resultypeXPath + ")");
		
		int paramFlag=0;
		for (long l = 1; l <= paramCount; l++) {
			String xPath = "/*/Configuration/*[%d]";
			String name = getStringByXPath(p, n, String.format("name(" + xPath + ")", l));
			if (name.equals(param[0])) {
				paramFlag=1;
			}
		}
		
		if(paramFlag==0 && !param[0].equals(resultTypeName)) {
			System.err.println("\n ERROR: Invalid parameter name: '" + param[0] + "'");
			describeMergeType(target, p, mergeType);
			System.exit(1);
		}
	}
	
	/**
	 * This function checks the arguments passed from the commandline
	 * 
	 * @param mergeType a merge type from the commandline
	 * @param args arguments from the commandline
	 * @throws Exception
	 */
	private static void checkArguments(String mergeType, String[] args) throws Exception {
		argsLength = args.length;

		if (argsLength < 8) {
			String type=(THREE_WAY_MERGE_TYPE.equals(mergeType)) ? THREE_WAY_CONCURRENT_MERGE_TYPE : mergeType;
			System.err.println("\n ERROR: Not enough arguments to '" + type + "'");
			describeMergeType(target, p, mergeType);
			System.exit(1);
		}

		firstParamIndex = getFirstParamIndex(args);
		paramCount = firstParamIndex < 0 ? 0 : (argsLength - firstParamIndex);
		resultFileLocation = firstParamIndex < 0 ? argsLength - 1 : (argsLength - paramCount) - 1;

		if (resultFileLocation % 2 == 1 || resultFileLocation < 8) {
			System.err.println("\n ERROR: Not enough arguments or missing result-file argument");
			describeMergeType(target, p, mergeType);
			System.exit(1);
		}
		
		if (THREE_WAY_MERGE_TYPE.equals(mergeType) && resultFileLocation > 8) {
			System.err.println("\n ERROR: A three way concurrent merge should not take more than three inputs");
			describeMergeType(target, p, mergeType);
			System.exit(1);
		}
	}

	private static int getFirstParamIndex(String[] args) {
		int result = -1;
		int i = -1;
		for (String s : args) {
			i++;
			if (s.contains("=")) {
				result = i;
				break;
			}
		}
		return result;
	}

	/**
     * Gets a string value from a node tree using an XPath
     *
     * @param p saxon processor
     * @param n node tree
     * @param xpath the xpath query
     * @return a string value corresponding to the XPath
     * @throws SaxonApiException Exception thrown by Saxon
     */
	private static String getStringByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {
		XPathSelector xps = p.newXPathCompiler().compile(xpath).load();
		xps.setContextItem(n);
		return xps.evaluateSingle().getStringValue();
	}

	/**
     * Gets a long value from a node tree using an XPath
     *
     * @param p saxon processor
     * @param n node tree
     * @param xpath the xpath query
     * @return a string value corresponding to the XPath
     * @throws SaxonApiException Exception thrown by Saxon
     */
	private static long getLongByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {
		XPathSelector xps = p.newXPathCompiler().compile(xpath).load();
		xps.setContextItem(n);
		return ((XdmAtomicValue) xps.evaluateSingle()).getLongValue();
	}

    /**
    * return HTTP GET method response for a request path 
    * @param path request path
    * @param target client info
    * @return Response
    */
	private static Response getResponse(String path, WebTarget target) {
		Response response;
		response = target.path(path).request().accept(MediaType.APPLICATION_XML).get();
		return response;
	}
}
